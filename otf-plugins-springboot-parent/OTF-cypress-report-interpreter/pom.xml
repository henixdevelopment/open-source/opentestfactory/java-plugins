<?xml version="1.0" encoding="UTF-8"?>
<!--

     Copyright (c) 2020 - 2023 Henix, henix.fr

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.opentestfactory.plugins</groupId>
        <artifactId>otf-plugins-springboot-parent</artifactId>
        <version>${revision}${sha1}${changelist}</version>
        <relativePath>..</relativePath>
    </parent>
    <artifactId>otf-cypress-report-interpreter</artifactId>
    <name>OTF-cypress-report-interpreter (${project.version})</name>
    <description>OTF Cypress report interpreter plugin</description>

    <properties>
        <license.name>Apache-V2</license.name>
        <maven.license.version>3.0</maven.license.version>
        <java.version>11</java.version>
    </properties>

    <distributionManagement>
        <repository>
            <id>squash-release-deploy-repo</id>
            <name>Squash releases deployment repo</name>
            <url>${deploy-repo.release.url}</url>
        </repository>
        <snapshotRepository>
            <id>squash-snapshot-deploy-repo</id>
            <name>Squash snapshots deployment repo</name>
            <url>${deploy-repo.snapshot.url}</url>
        </snapshotRepository>
    </distributionManagement>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.opentestfactory</groupId>
                <artifactId>otf-java-bom</artifactId>
                <version>${project.version}</version>
                <scope>import</scope>
                <type>pom</type>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.opentestfactory</groupId>
            <artifactId>otf-java-bom</artifactId>
            <version>${project.version}</version>
            <type>pom</type>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-jersey</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.junit.vintage</groupId>
                    <artifactId>junit-vintage-engine</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-autoconfigure</artifactId>
        </dependency>
        
        <!-- Non springboot compile/runtime dependencies -->
        <dependency>
            <groupId>com.nimbusds</groupId>
            <artifactId>nimbus-jose-jwt</artifactId>
        </dependency>
        <dependency>
            <groupId>org.bouncycastle</groupId>
            <artifactId>bcpkix-fips</artifactId>
            <version>1.0.4</version>
        </dependency>
        <dependency>
            <groupId>org.opentestfactory</groupId>
            <artifactId>TFMessages</artifactId>
        </dependency>
        <dependency>
            <groupId>org.opentestfactory</groupId>
            <artifactId>tf-microservice-components</artifactId>
        </dependency>
        <dependency>
            <groupId>org.opentestfactory.plugins</groupId>
            <artifactId>otf-report-interpreter-base</artifactId>
            <version>${project.version}</version>
        </dependency>

        <!-- Test dependencies. -->
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.8.0</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <!-- version controlled from otf-plugins-springboot-parent to override springboot's dependencyManagement -->
        </dependency>
        <dependency>
            <groupId>org.junit.vintage</groupId>
            <artifactId>junit-vintage-engine</artifactId>
            <version>5.7.0</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.opentestfactory</groupId>
            <artifactId>test-utils</artifactId>
        </dependency>
    </dependencies>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>com.mycila</groupId>
                    <artifactId>license-maven-plugin</artifactId>
                    <version>${maven.license.version}</version>
                    <configuration>
                        <header>../../license_conf/${license.name}-license-header.txt</header>
                        <excludes>
                            <exclude>**/*</exclude>
                            <exclude>**/*.*</exclude>
                        </excludes>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>build-helper-maven-plugin</artifactId>
                    <executions>
                        <execution>
                            <id>timestamp-property</id>
                            <goals>
                                <goal>timestamp-property</goal>
                            </goals>
                            <phase>validate</phase>
                            <configuration>
                                <name>license.current.year</name>
                                <pattern>yyyy</pattern>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>release-logic</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.gmaven</groupId>
                        <artifactId>gmaven-plugin</artifactId>
                        <version>1.5</version>
                        <executions>
                            <execution>
                                <id>release-logic</id>
                                <goals>
                                    <goal>execute</goal>
                                </goals>
                                <phase>generate-sources</phase>
                                <configuration>
                                    <source>src/main/scripts/preRelease.groovy</source>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
